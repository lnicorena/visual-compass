/*
 * ColorClasses.h
 *
 *  Created on: 28/05/2016
 *      Author: leonardo
 */

#ifndef COLORCLASSES_H_
#define COLORCLASSES_H_

// http://opencv-srf.blogspot.com.br/2010/09/object-detection-using-color-seperation.html

class ColorClasses {
public:
	ColorClasses();
	virtual ~ColorClasses();

	int value;
};

#endif /* COLORCLASSES_H_ */
