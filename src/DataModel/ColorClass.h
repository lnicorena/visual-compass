/*
 * ColorClass.h
 *
 *  Created on: 28/05/2016
 *      Author: leonardo
 */

#ifndef COLORCLASS_H_
#define COLORCLASS_H_

#include <opencv2/imgproc/imgproc.hpp>
#include "Color.h"
// http://opencv-srf.blogspot.com.br/2010/09/object-detection-using-color-seperation.html

class ColorClass {
public:
	ColorClass();
	virtual ~ColorClass();

	void setMainColor(int hue, int sat, int val);
	void setSecundColor(int hue, int sat, int val);

	Color mainColor1C;
	Color mainColor2C;
	Color mainColor3C;

	Color secColor1C;
	Color secColor2C;
	Color secColor3C;

	/*
	int main_color_hue;
	int main_color_sat;
	int main_color_val;

	int sec_color_hue;
	int sec_color_sat;
	int sec_color_val;
*/

	void storeImage(cv::Mat img, cv::Mat img_ori, int x1, int y1, int x2, int y2);
	cv::Mat img_roi;
	cv::Mat img_roi_ori;
	int roi_x_ini;
	int roi_x_end;
	int roi_y_ini;
	int roi_y_end;
};

#endif /* COLORCLASS_H_ */
