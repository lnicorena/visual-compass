/*
 * Processor.cpp
 *
 *  Created on: 28/05/2016
 *      Author: leonardo
 */

#include "opencv2/highgui/highgui.hpp"
#include <iostream>
#include "../DataModel/Params.h"

#include "ColorProcessing.h"
#include "Processor.h"

using namespace std;
using namespace cv;

Processor::Processor() {
	// TODO Auto-generated constructor stub

}

Processor::~Processor() {
	// TODO Auto-generated destructor stub
}


void Processor::execute(){

	string img_src = "/home/leonardo/Imagens/fish.png";
	//Processor::getImage(img_src, true);

	Mat image, image2, hsvimg;
	image = this->getImage(img_src);
	ColorProcessing * color_processor;

	//color_processor->testColorspace();
	color_processor->testHsvChannels();
	//color_processor->testOpeningClosing();

}

void Processor::transform(Mat& image, Mat& output, string format){

	ColorProcessing * color_processor;

	//image = imread("/home/leonardo/workspace/VisualCompass/images/case1.jpg");
	//imshow("BGR Original Image", image);
	Mat hsv_image;

	color_processor->bgrToHsv(image, hsv_image);

	color_processor->imageOpening(hsv_image, Params::imClosingK);
	color_processor->imageClosing(hsv_image, Params::imClosingK);

	color_processor->colorReduction(hsv_image, Params::colorRedFactor);

	if (format == "hsv"){
		hsv_image.copyTo(output);
	} else {
		color_processor->hsvToBgr(hsv_image, output);
	}

}



Mat Processor::getImage(string img_src, bool show_img){

	Mat image;
	image = imread( img_src, 1 /* 1 returns image in 3 channel BGR colorspace*/);

	if( !image.data ){
	  cout << "No image data \n";
	}

	if(show_img){
		namedWindow( "Display Image", CV_WINDOW_AUTOSIZE );
		imshow( "Display Image", image );
	}
	return image;
}


