/*
 * Processor.h
 *
 *  Created on: 28/05/2016
 *      Author: leonardo
 */

#ifndef PROCESSOR_H_
#define PROCESSOR_H_


using namespace std;
using namespace cv;

class Processor {
public:
	Processor();
	virtual ~Processor();

	cv::Mat getImage(string img_src, bool show_img = false);

	void execute();
	void transform(Mat& image, Mat& output, string format);
};

#endif /* PROCESSOR_H_ */
