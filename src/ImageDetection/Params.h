/*
 * Params.h
 *
 *  Created on: 28/05/2016
 *      Author: leonardo
 */

#ifndef PARAMS_H_
#define PARAMS_H_

class Params {
public:
	Params();
	virtual ~Params();

	static const int pxWidth = 800;
	static const int pxHeight = 600;

	static const int neckAngle = 0;



};

#endif /* PARAMS_H_ */
